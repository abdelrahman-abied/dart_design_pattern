import 'package:design_patterns/strategy_pattern/duck_toy.dart';
import 'package:design_patterns/strategy_pattern/injured_duck.dart';

void main(List<String> arguments) {
// strategy pattern
  DuckToy duckToy = DuckToy();
  duckToy.perform();
  print("=============");
  InjuredDuck injuredDuck = InjuredDuck();
  injuredDuck.perform();
}
