import 'package:design_patterns/strategy_pattern/duck.dart';

class InjuredDuck extends Duck {
  @override
  String display() {
    return "This is injured duck";
  }
}
