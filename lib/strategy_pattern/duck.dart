import 'package:design_patterns/strategy_pattern/fly_behaviour.dart';
import 'package:design_patterns/strategy_pattern/fly_with_wings.dart';

abstract class Duck {
  late int _id;
  late String _name;
  static int _counter = 0;
  late FlyBehaviour flyBehaviour;
  Duck() {
    _id = _counter++;
    _name = "Batta#$_id";
    flyBehaviour = FlyWithWings();
  }
  String swim() {
    return "this bird can swim";
  }

  String display();
  void perform() {
    print("Hi,there$_name");
    print(swim());
    print(display());
    print(flyBehaviour.fly());
  }
}
