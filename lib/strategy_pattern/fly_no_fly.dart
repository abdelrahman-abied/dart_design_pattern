import 'package:design_patterns/strategy_pattern/fly_behaviour.dart';

class FlyNoFly implements FlyBehaviour {
  @override
  String fly() {
    return "can't fly";
  }
}
