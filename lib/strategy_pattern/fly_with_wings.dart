import 'package:design_patterns/strategy_pattern/fly_behaviour.dart';

class FlyWithWings implements FlyBehaviour {
  @override
  String fly() {
    return "fly with wings";
  }
}
