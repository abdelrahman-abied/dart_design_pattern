import 'package:design_patterns/strategy_pattern/duck.dart';
import 'package:design_patterns/strategy_pattern/fly_no_fly.dart';

class DuckToy extends Duck {
  DuckToy() {
    flyBehaviour = FlyNoFly();
  }
  @override
  String display() {
    return "This is toy duck";
  }
}
